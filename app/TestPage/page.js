import Image from "next/image";
import TextBox from "../Components/textbox";
import Button from "../Components/button";
import TopBar from "../Components/topbar";
import { FeatureBox, FeatureBoxGrid } from "../Components/featureBox";
import Footer from "../Components/customFooter";
export default function TestPage() {
  return (
    <div className="mt-12">
      <TopBar></TopBar>
      <TextBox variant="special">Upgrade your</TextBox>
      <TextBox variant="special">Dear Diary</TextBox>
      <TextBox variant="h1">Test</TextBox>
      <TextBox variant="h2">Test</TextBox>
      <TextBox variant="h3">Test</TextBox>
      <TextBox variant="h4">Test</TextBox>
      <TextBox variant="h5">Test</TextBox>
      <TextBox variant="p">Test</TextBox>

      <Button>Button</Button>
      <Button variant="page">Button</Button>
      <FeatureBoxGrid></FeatureBoxGrid>
      <Footer></Footer>
    </div>
  );
}
