import React from "react";
import TextBox from "../textbox";
import Button from "../button";

export default function FinalSection() {
  return (
    <div className="w-screen flex justify-center bg-slate-200 dark:bg-slate-800 h-[400px]">
      <div className="max-w-[1200px] h-full flex flex-col gap-3 items-center justify-center">
        <TextBox variant="h2">Still not convinced?</TextBox>
        <Button variant="page">Try it out, it's FREE!</Button>
      </div>
    </div>
  );
}
