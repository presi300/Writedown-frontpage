import React from "react";

import TextBox from "../textbox";

export function CathPhrase1() {
  return (
    <div className="text-center">
      <TextBox variant="h3">Still using & paper?</TextBox>
      <h4 className="text-[30px] text-slate-400">
        Cmon now, it's not the 90s anymore
      </h4>
    </div>
  );
}

export function MainTitle() {
  return (
    <div>
      <TextBox variant="special">Upgrade your</TextBox>
      <TextBox variant="special">Dear Diary</TextBox>
      <h2 className="text-slate-600 dark:text-text-dark text-center text-[12px] sm:text-[24px]">
        The easiest way to write down
      </h2>
    </div>
  );
}
