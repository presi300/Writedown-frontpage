import React from "react";
import TextBox from "../textbox";
import { RiGithubFill } from "react-icons/ri";
import { RiNextjsFill } from "react-icons/ri";
import { RiFirebaseFill } from "react-icons/ri";
import { RiTailwindCssFill } from "react-icons/ri";

// TODO: ask nayamamarshe if these are links or not
import Image from "next/image";

export default function UsedTechnologies({}) {
  return (
    <div className="w-screen h-[400px] flex justify-center">
      <div className="max-w-[1200px] flex flex-col h-full items-center justify-center text-center">
        <TextBox variant="h3">
          Built on the foundation of trust and open source
        </TextBox>
        <div className="flex gap-6 sm:gap-12 mt-5">
          <RiGithubFill
            className="w-[40px] sm:w-[60px]"
            size={60}
          ></RiGithubFill>
          <RiNextjsFill
            className="w-[40px] sm:w-[60px]"
            size={60}
          ></RiNextjsFill>
          <RiFirebaseFill
            className="w-[40px] sm:w-[60px]"
            size={60}
          ></RiFirebaseFill>
          <RiTailwindCssFill
            className="w-[40px] sm:w-[60px]"
            size={60}
          ></RiTailwindCssFill>
        </div>
      </div>
    </div>
  );
}
