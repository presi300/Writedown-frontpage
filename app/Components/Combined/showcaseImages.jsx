import React from "react";
import Image from "next/image";
import TextBox from "../textbox";
import Button from "../button";

export default function ShowcaseImages() {
  return (
    <div className="w-screen flex justify-center">
      <div className="grid grid-rows-2 max-w-[1200px] gap-24 my-32">
        {/* Showcase Image 1 */}
        <div className="flex flex-col-reverse sm:flex-row items-center justify-center w-full">
          <div className="text-center sm:text-start flex flex-col gap-3 mt-7 sm:mt-7 sm:px-6">
            {" "}
            <TextBox variant="h3">Easy to use, easy to write</TextBox>
            <TextBox variant="p">
              With writedown it becomes easier to jot down your thoughts quickly
              and in an efficient manner.
            </TextBox>
            <Button variant="page">Try it out!</Button>
          </div>

          <div className="w-full flex justify-center sm:justify-end sm:pr-6">
            <Image
              width={630}
              height={630}
              className="customShadow dark:customShadowDark rounded-[33px]  max-w-[630px] xl:max-w-[550px] w-[90vw] sm:w-[50vw]"
              src={"/Images/showcaseImg1.png"}
            ></Image>
          </div>
        </div>
        {/* Showcase Image 2 */}
        <div className="flex flex-col-reverse sm:flex-row-reverse items-center justify-center w-full">
          <div className="text-center sm:text-start flex flex-col gap-3 mt-7 mr-5 sm:mt-0  sm:px-6">
            {" "}
            <TextBox variant="h3">Easy to use, easy to write</TextBox>
            <TextBox variant="p">
              With writedown it becomes easier to jot down your thoughts quickly
              and in an efficient manner.
            </TextBox>
            <Button variant="page">Try it out!</Button>
          </div>

          <div className="w-full flex justify-center sm:justify-start sm:pl-6">
            <Image
              width={630}
              height={630}
              className="customShadow dark:customShadowDark rounded-[33px] max-w-[630px] xl:max-w-[550px] w-[90vw] sm:w-[50vw]"
              src={"/Images/showcaseImg1.png"}
            ></Image>
          </div>
        </div>
      </div>
    </div>
  );
}
