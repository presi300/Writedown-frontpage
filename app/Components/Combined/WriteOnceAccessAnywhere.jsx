import React from "react";
import TextBox from "../textbox";
import Button from "../button";
import Image from "next/image";

// This is the 2nd to last section with the "write, once access anywhere" catchphrase

export default function WriteOnceAccessAnywhere() {
  return (
    <div className="w-screen flex justify-center bg-slate-200 dark:bg-slate-800 h-[700px]">
      <div className="max-w-[1200px] h-full flex items-center px-6 justify-center gap-24">
        <div className="max-w-[90%] sm:max-w-[50%] ml-3">
          {" "}
          <TextBox variant="h2">write once,</TextBox>
          <TextBox variant="h2">access anywhere</TextBox>
          <TextBox variant="p">
            writedown lets you export your notes to PDF, Markdown, HTML and
            Text. Print, share or read. writedown gives you the ultimate
            control.
          </TextBox>
          <div className="mt-4">
            <Button variant="page">Try it now!</Button>
          </div>
        </div>
        <div className="hidden sm:block min-w-[300px] w-[300px]">
          <Image
            className=""
            width={300}
            height={310}
            src={"/Images/formats.png"}
          ></Image>
        </div>
      </div>
    </div>
  );
}
