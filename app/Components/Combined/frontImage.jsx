import React from "react";
import Button from "../button";
import Image from "next/image";
import TextBox from "../textbox";

export default function FrontImage() {
  return (
    <div className="w-full flex flex-col gap-12 items-center">
      {/* Desktop light */}
      <Image
        className="max-w-[1000px] w-[85vw] dark:sm:hidden hidden sm:block"
        width={1200}
        height={900}
        src={"/Images/image1.png"}
      ></Image>
      {/* Mobile light */}
      <Image
        className="block dark:hidden sm:hidden dark:sm:hidden max-w-[300px] w-[90vw] rounded-[12px] customShadow"
        width={500}
        height={900}
        src={"/Images/image1-mbl.png"}
      ></Image>
      {/* Desktop dark */}
      <Image
        className="max-w-[1000px] dark:sm:block hidden  w-[80vw] "
        width={1200}
        height={900}
        src={"/Images/image1-dark.png"}
      ></Image>
      {/* Mobile dark */}
      <Image
        className="dark:block hidden sm:hidden dark:sm:hidden max-w-[300px] w-[90vw] rounded-[12px] customShadowDark"
        width={500}
        height={900}
        src={"/Images/image1-dark-mbl.png"}
      ></Image>
    </div>
  );
}
