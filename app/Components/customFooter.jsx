import React from "react";
import TextBox from "./textbox";
import { FaGithub } from "react-icons/fa";
import Button from "./button";

export default function Footer() {
  return (
    <footer className="bottom-0 left-0 right-0 bg-midnight-300 h-[80px] grid grid-cols-2 lg:grid-cols-3 px-[50px]">
      {/* things on the left */}
      <div className="flex h-full items-center">
        <div className="text-[12px] lg:text-[18px] text-text-dark">
          ©2024 <b>writedown.</b> All rights reserved.
        </div>
      </div>
      {/* Things in the middle */}
      <div className=" w-full h-full items-center justify-center text-text-dark hidden lg:flex">
        <FaGithub size={40}></FaGithub>
      </div>
      {/* Things on the right */}
      <div className="flex w-full h-full items-center justify-end gap-1 md:gap-5">
        <p className="text-[10px] sm:text-[16px] text-text-dark">
          Like what you see?
        </p>
        <button
          className="px-[10px] py-[3px] md:px-[20px] md:py-[7px] rounded-full dark:hover:bg-dusk-500 transition-colors  border-dusk-50 border-[1px]  bg-midnight-300"
          href="#"
        >
          <p className="text-[10px] sm:text-[16px] font-regular text-text-dark">
            Support the project!
          </p>{" "}
        </button>
      </div>
    </footer>
  );
}
