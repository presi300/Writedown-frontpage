import React from "react";
import { CiGlobe } from "react-icons/ci";
import TextBox from "./textbox";

export function FeatureBox({
  icon = <CiGlobe size={35}></CiGlobe>,
  bigText = "loremIpsum",
  smallText = "loremipsumLoremIpsum",
}) {
  return (
    <div className="flex  justify-center items-center w-full h-[75px] gap-[15px] md:flex-col md:h-[220px] md:w-[280px] md:gap-[7px] md:bg-chalk-200 md:hover:bg-chalk-75 md:hover:dark:bg-slate-600 md:dark:bg-slate-700 rounded-[12px] hover:scale-110 transition-all">
      <div className="bg-chalk-200 md:bg-transparent md:dark:bg-transparent dark:bg-slate-700 text-text-light dark:text-text-dark p-3.5 rounded-[12px] md:p-0 md:rounded-[0px] ">
        {icon}
      </div>
      <div className="text-center md:h-auto h-full">
        <TextBox variant="h3">{bigText}</TextBox>
        <TextBox variant="h5"> {smallText}</TextBox>
      </div>
    </div>
  );
}
export function FeatureBoxGrid() {
  return (
    <div className="w-screen flex justify-center mb-44">
      <div className="max-w-[1000px]">
        <div className="w-full flex flex-col gap-12  md:grid md:grid-cols-2 md:grid-rows-3 lg:grid-cols-3 lg:grid-rows-2">
          <FeatureBox></FeatureBox>
          <FeatureBox></FeatureBox>
          <FeatureBox></FeatureBox>
          <FeatureBox></FeatureBox>
          <FeatureBox></FeatureBox>
          <FeatureBox></FeatureBox>
        </div>
      </div>
    </div>
  );
}
