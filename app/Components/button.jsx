import React from "react";
import { children } from "react";
import TextBox from "./textbox";

export default function Button({
  variant = "ui",
  children,
  href = "#",
  backgroundCol = "#FFFFFF",
}) {
  return (
    <div>
      {variant === "ui" && (
        <button
          className="px-[20px] py-[7px] rounded-full transition-colors hover:bg-chalk-75 dark:hover:bg-dusk-500 border-midnight-400 dark:border-dusk-50 border-[1px] bg-chalk-50 dark:bg-midnight-300"
          href={href}
        >
          <TextBox variant="ui">{children}</TextBox>
        </button>
      )}
      {variant === "page" && (
        <button
          href={href}
          className="px-[30px] hover:scale-105 transition-transform py-[7px] rounded-full bg-dusk-400 dark:bg-midnight-200"
        >
          <p className="text-[16px] font-regular text-text-dark">{children}</p>
        </button>
      )}
      {variant === "custom" && (
        <button
          href={href}
          className="px-[30px] py-[7px] rounded-full"
          style={{ background: backgroundCol }}
        >
          <p className="text-[16px] font-regular text-text-dark">{children}</p>
        </button>
      )}
    </div>
  );
}
