"use client";
import React from "react";
import Button from "./button";
import ThemeSwitcher from "./themeSwitcher";
import { useState, useEffect } from "react";

export default function TopBar() {
  // Logic for changing the scrollbar whenever it's not at the very top
  const [isAtTop, setIsAtTop] = useState(true);
  useEffect(() => {
    const handleScroll = () => {
      const scrollTop =
        window.pageYOffset || document.documentElement.scrollTop;
      setIsAtTop(scrollTop === 0);
    };
    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  //TODO: figure out a better way to scale the topbar, currently it just hides the middle section
  return (
    <header
      className="fixed top-1 bg-chalk-50 dark:bg-midnight-300 left-1 right-1 h-[63px] rounded-full grid grid-cols-2 sm:grid-cols-3 px-[20px] transition-all dark:border-dusk-50 border-midnight-400 z-30"
      style={{
        borderWidth: isAtTop ? "0px" : "1px",
        top: isAtTop ? "0px" : "4px",
        left: isAtTop ? "0px" : "4px",
        right: isAtTop ? "0px" : "4px",
        borderRadius: isAtTop ? "0px" : "9999px",
      }}
    >
      {/* Logo */}
      <div className="flex w-full h-full  justify-start items-center">
        <div>
          <h3 className="text-xl font-semibold text-slate-950 dark:invert">
            writedown.
          </h3>
        </div>
      </div>
      {/* Middle buttons */}
      <div className="hidden sm:flex w-full h-full  justify-center items-center gap-5">
        <a
          className="text-text-light dark:text-text-dark hover:underline text-[16px]"
          href="#"
        >
          Product
        </a>
        <a
          className="text-text-light dark:text-text-dark hover:underline text-[16px]"
          href="#"
        >
          About
        </a>
        <a
          className="text-text-light dark:text-text-dark hover:underline text-[16px]"
          href="#"
        >
          GitHub
        </a>
      </div>
      {/* Right buttons */}
      <div className="flex w-full h-full  items-center justify-end gap-3 ">
        <ThemeSwitcher></ThemeSwitcher>
        <div className="xs:hidden">
          <Button>Write now!</Button>
        </div>
      </div>
    </header>
  );
}
