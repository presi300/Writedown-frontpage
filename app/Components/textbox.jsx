import React from "react";
import { children } from "react";
import { Syne } from "next/font/google";

const syne = Syne({ subsets: ["latin"], weight: ["700"] });

export default function TextBox({ children, variant = "p" }) {
  return (
    <div>
      {variant === "special" && (
        <h1
          className={`${syne.className} font-bold text-[40px] sm:text-[65px] text-center text-text-light dark:text-text-dark`}
        >
          {children}
        </h1>
      )}
      {variant === "h1" && (
        <h1 className="font-bold text-[38px] sm:text-[52px] text-text-light dark:text-text-dark">
          {children}
        </h1>
      )}
      {variant === "h2" && (
        <h2 className="font-bold text-[32px] sm:text-[42px] text-text-light dark:text-text-dark">
          {children}
        </h2>
      )}
      {variant === "h3" && (
        <h3 className="font-semibold text-[24px] sm:text-[36px] text-text-light dark:text-text-dark">
          {children}
        </h3>
      )}{" "}
      {variant === "h4" && (
        <h4 className="font-medium text-[20px] sm:text-[28px] text-text-light dark:text-text-dark">
          {children}
        </h4>
      )}
      {variant === "h5" && (
        <h5 className="text-[22px] sm:text-[18px] text-text-light dark:text-text-dark">
          {children}
        </h5>
      )}
      {variant === "p" && (
        <p className="text-[18px] text-text-light dark:text-text-dark">
          {children}
        </p>
      )}
      {variant === "ui" && (
        <p className="text-[16px] font-regular text-text-light dark:text-text-dark">
          {children}
        </p>
      )}
    </div>
  );
}
