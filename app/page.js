import TextBox from "./Components/textbox";
import TopBar from "./Components/topbar";
import Button from "./Components/button";
import FrontImage from "./Components/Combined/frontImage";
import { MainTitle, CathPhrase1 } from "./Components/Combined/textFields";
import ShowcaseImages from "./Components/Combined/showcaseImages";
import { FeatureBoxGrid } from "./Components/featureBox";
import WriteOnceAccessAnywhere from "./Components/Combined/WriteOnceAccessAnywhere";
import UsedTechnologies from "./Components/Combined/usedTechnologies";
import FinalSection from "./Components/Combined/finalSection";
import Footer from "./Components/customFooter";
export default function Home() {
  return (
    <div className="mt-32 max-w-screen overflow-hidden">
      <TopBar></TopBar>
      <div className="text-center flex flex-col gap-12 mb-12">
        <MainTitle></MainTitle>
        <Button variant="page">Write now!</Button>
      </div>
      <FrontImage></FrontImage>
      <div className="text-center my-32">
        {/* No, I am not gonna fix the typo -presi300 */}
        <CathPhrase1></CathPhrase1>
        <ShowcaseImages></ShowcaseImages>
        <TextBox variant="h2">Features, plenty.</TextBox>
        <TextBox variant="h2">Looks, fancy ✨</TextBox>
      </div>

      <FeatureBoxGrid></FeatureBoxGrid>
      <WriteOnceAccessAnywhere></WriteOnceAccessAnywhere>
      <UsedTechnologies></UsedTechnologies>
      <FinalSection></FinalSection>
      <Footer></Footer>
    </div>
  );
}
